# Individual Project 2 [![pipeline status](https://gitlab.com/dukeaiml/IDS721/yc557-ip-2/badges/main/pipeline.svg)](https://gitlab.com/dukeaiml/IDS721/yc557-ip-2/-/commits/main)
> Oliver Chen (yc557)

This project implements a simple REST API/web service in Rust, containerized using Docker for seamless deployment. It includes CI/CD pipeline files to automate the build, test, and deployment processes, ensuring efficient continuous delivery of the Rust microservice.

## Rust Microservice Functionality
This Rust microservice utilizes the Actix web framework to provide a simple REST API, offering two endpoints. The root endpoint greets users and provides instructions on using the service, while the `'/triple'` endpoint calculates the triple power of a provided number. Users can access this functionality by specifying the number as a query parameter in the URL. The service efficiently handles requests by parsing the number parameter, computing the triple power, and returning a JSON response containing both the original number and its triple. Powered by Actix web and built for seamless deployment using Docker, this microservice ensures reliable and scalable performance for calculating the triple power of numbers via a user-friendly API.

Here are the two endpoints mentioned:
```rust
#[get("/")]
async fn index() -> impl Responder {
    "Hello there! \n You can use the endpoint /triple?number=<yournumber> to calculate the triple power of a number."
}

#[get("/triple")]
async fn triple(web::Query(info): web::Query<HashMap<String, String>>) -> impl Responder {
    match info.get("number") {
        Some(num_str) => {
            match num_str.parse::<i32>() {
                Ok(number) => {
                    let triple = number * number * number;
                    let response = TripleResponse { number, triple };
                    HttpResponse::Ok().json(response)
                }
                Err(_) => HttpResponse::BadRequest().body("Invalid number provided"),
            }
        }
        None => HttpResponse::BadRequest().body("Missing number query parameter"),
    }
}
```

## Docker Configuration
1. Write the Dockerfile as following. This Dockerfile sets up a builder stage for building a Rust application using the official Rust 1.75 image as the base image. It first specifies the working directory inside the container where the project will be located. Then, it copies the entire content of the local directory into the container's working directory. Afterward, it runs `cargo build --release` to build the project with optimized code. The Dockerfile also exposes port 8080 for outside access, which is typically used by Actix-Web, a popular Rust web framework. Finally, it defines the default command to run when the container starts, using `cargo run`. This Dockerfile enables efficient containerization and deployment of Rust applications, providing a streamlined workflow for building and running Rust projects in a containerized environment.
```dockerfile
# Use the official Rust 1.75 image as the base image for the builder stage
FROM rust:1.75 AS builder

# Set the working directory inside the container
WORKDIR /usr/src/ip-2

# Change the user to root for copying files (optional, depending on your needs)
USER root

# Copy the entire content of the local directory into the container's working directory
COPY . .

# Build the project using Cargo with the --release flag for optimized code
RUN cargo build --release

# Expose port 8080 to the outside world (used by Actix-Web)
EXPOSE 8080

# Define the default command to run when the container starts, using 'cargo run'
CMD cargo run

```

2. Build the Docker image by running the following command:
```bash
sudo docker build -t ip-2 .
```

3. Run the following command to run the Docker container locally:
```bash
sudo docker run -p 8080:8080 ip-2
```

4. Then, the application should be able to be accessed on http://localhost:8088/.

## CI/CD Pipeline
1. Write the `.gitlab-ci.yml` file as the following. This GitLab CI/CD YAML file defines a pipeline with a single stage named `build`. It also declares variables to be used across jobs, such as the image tag, Docker host, and Docker driver. The `build` job utilizes the Docker-in-Docker service for building Docker images. Before running the script, it logs in to the GitLab Container Registry using the provided credentials. During the script execution, it builds a Docker image tagged as `rust-microservice`, runs it as a detached container with port 8080 exposed, and lists the containers using the `docker ps -a` command. Finally, in the after-script section, it prints a message indicating that the Rust microservice is now running on port 8080. This YAML file orchestrates the building and deployment of a Rust microservice within a CI/CD pipeline, ensuring seamless integration and testing processes.
```yml
# Define stages for the pipeline
stages:
  - build

# Define variables that can be used across jobs
variables:
  IMAGE_TAG: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG
  DOCKER_HOST: tcp://docker:2375
  DOCKER_DRIVER: overlay2

build:
  stage: build
  image: docker:stable  # Use the Docker image for running Docker commands
  services:
    - docker:dind  # Enable Docker-in-Docker service for building Docker images
  before_script:
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" "$CI_REGISTRY"  # Log in to the GitLab Container Registry
  script:
    - docker build -t rust-microservice .
    - docker run -d -p 8080:8080 rust-microservice
    - docker ps -a
  after_script:
    - echo "Rust microservice is now running on port 8080"
``` 

2. Push the yml file to the remote repository and check the pipeline job on GitLab.


## Screenshots
Here are the terminal outputs after the Docker setup (some packages omitted in the screenshots):
![docker-1](./images/docker-1.png)
![docker-2](./images/docker-2.png)
![docker-3](./images/docker-3.png)

We can check out the newly created Docker image on Docker Desktop:
![docker-desktop](./images/docker-desktop.png)

Then, we can test the simple Rust web application.
![web-1](./images/web-1.png)
![web-2](./images/web-2.png)

We can also see the CI/CD job passes on GitLab, which can also be seen on the badge on the top of this README file.
![cicd](./images/cicd.png)

## Demo Video
You can also check out the demo video [here](https://gitlab.com/dukeaiml/IDS721/yc557-ip-2/-/blob/main/images/demo.mov?ref_type=heads).
